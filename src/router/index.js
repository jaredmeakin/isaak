import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home'
import ProjectUTA from '@/pages/ProjectUTA'
import ProjectQuilt from '@/pages/ProjectQuilt'
import ProjectClean from '@/pages/ProjectClean'
import ProjectHammy from '@/pages/ProjectHammy'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    { path: '/', name: 'Home', component: Home },
    { path: '/projects/utah-transit-authority', name: 'Project_1', component: ProjectUTA },
    { path: '/projects/quiltback', name: 'Project_2', component: ProjectQuilt },
    { path: '/projects/quickclean', name: 'Project_3', component: ProjectClean },
    { path: '/projects/hammysack', name: 'Project_4', component: ProjectHammy }
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})
